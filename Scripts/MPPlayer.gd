#	This file is part of An Untitled SCP Game.
#
#	An Untitled SCP Game is free software: you can redistribute it
#	and/or modify it under the terms of the GNU General Public License
#	as published by the Free Software Foundation, either version 3
#	of the License, or (at your option) any later version.
#
#	An Untitled SCP Game is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty
#	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#	See the GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with Untitled SCP Game. If not, see <https://www.gnu.org/licenses/>. 

extends KinematicBody

onready var cam = $Camera
var speed = 6 #players speed
var mouse_sense = 1 #players mouse sencivity
var direction = Vector3()
var velocity = Vector3()
var gravity = 6


func _ready():
	#hides the cursor
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _input(event):
	#get mouse input for camera rotation
	if event is InputEventMouseMotion:
		rotation.y += deg2rad(-event.relative.x * mouse_sense)
		cam.rotation.x += deg2rad(-event.relative.y * mouse_sense)
		cam.rotation.x = clamp(cam.rotation.x, deg2rad(-90), deg2rad(90))


remote func _set_pos(position):
	global_transform.origin = position


func _physics_process(delta):
	direction = Vector3()

	if Input.is_action_pressed("move_forwards"):
		direction -= transform.basis.z
	elif Input.is_action_pressed("move_backwards"):
		direction += transform.basis.z
	if Input.is_action_pressed("move_left"):
		direction -= transform.basis.x
	elif Input.is_action_pressed("move_right"):
		direction += transform.basis.x

	direction = direction.normalized()

	velocity.x = direction.x * speed
	velocity.z = direction.z * speed
	velocity.y -= gravity * delta

	if direction != Vector3():
		if is_network_master():
			velocity = move_and_slide(velocity, Vector3.UP)
		rpc_unreliable("_set_pos", global_transform.origin)
