#	This file is part of An Untitled SCP Game.
#
#	An Untitled SCP Game is free software: you can redistribute it
#	and/or modify it under the terms of the GNU General Public License
#	as published by the Free Software Foundation, either version 3
#	of the License, or (at your option) any later version.
#
#	An Untitled SCP Game is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty
#	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#	See the GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with Untitled SCP Game. If not, see <https://www.gnu.org/licenses/>. 

extends Node2D

var peer
var timer_limit = 2.0
var timer = 0.0
var game
var is_server_running : bool = false


func _ready():
# warning-ignore:return_value_discarded
	get_tree().connect("network_peer_connected", self, "_connected")


func _on_HostButton_pressed():
	peer = NetworkedMultiplayerENet.new()
	peer.create_server(6969,2)
	get_tree().set_network_peer(peer)
	$Control.hide()
	$BG.hide()
	$gradient2.hide()
	$TempLogo.hide()
	$Version.hide()
	$Control2.show()
	is_server_running = true


func _on_JoinButton_pressed():
	peer = NetworkedMultiplayerENet.new()
	peer.create_client("127.0.0.1", 6969)
	get_tree().set_network_peer(peer)
	game = "res://Scenes/MPTest.tscn"
# warning-ignore:return_value_discarded
	get_tree().change_scene(game)


func _connected(client_id):
	GameRun.user_id = client_id
	game = "res://Scenes/MPTest.tscn"
# warning-ignore:return_value_discarded
	get_tree().change_scene(game)


func _on_SingleButton_pressed():
	game = "res://Scenes/Test.tscn"
# warning-ignore:return_value_discarded
	get_tree().change_scene(game)


func _on_CloseButton_pressed():
	get_node("Control/Container").show()
	get_node("Control/Container2").hide()
	get_node("gradient").hide()
	get_node("Version").show()


func _on_StartButton_pressed():
	$Control/Container2.show()
	$Control/Container.hide()
	$gradient.show()
	$Version.hide()


func _on_QuitButton_pressed():
	get_tree().quit()


func _on_OptionsButton_pressed():
	$Control/Container.hide()
	$Control/Control.show()


func _on_OptCloseButton_pressed():
	$Control/Container.show()
	$Control/Control.hide()


func _on_CheckButton_pressed():
	GameRun.show_counter += 1


func _process(_delta):
	if GameRun.show_counter == 2:
		GameRun.show_counter = 0

	if GameRun.show_counter == 1:
		$FPSCounter.show()

	if GameRun.show_counter == 0:
		$FPSCounter.hide()


	if is_server_running == true and Input.is_action_just_pressed("ui_cancel"):
		peer.stop_server()
