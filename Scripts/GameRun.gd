#	This file is part of An Untitled SCP Game.
#
#	An Untitled SCP Game is free software: you can redistribute it
#	and/or modify it under the terms of the GNU General Public License
#	as published by the Free Software Foundation, either version 3
#	of the License, or (at your option) any later version.
#
#	An Untitled SCP Game is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty
#	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#	See the GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with Untitled SCP Game. If not, see <https://www.gnu.org/licenses/>. 

extends Node

var sensevity : float = 1.0
var user_id = -1
var show_counter = 0
var show_mouse = true
