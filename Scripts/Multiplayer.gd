#	This file is part of An Untitled SCP Game.
#
#	An Untitled SCP Game is free software: you can redistribute it
#	and/or modify it under the terms of the GNU General Public License
#	as published by the Free Software Foundation, either version 3
#	of the License, or (at your option) any later version.
#
#	An Untitled SCP Game is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty
#	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#	See the GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with Untitled SCP Game. If not, see <https://www.gnu.org/licenses/>. 

extends Spatial

onready var player_pos_1 = $Spawn1
onready var player_pos_2 = $Spawn2


func _ready():
	var player1 = preload("res://Scenes/MPPlayer2.tscn").instance()
	player1.set_name(str(get_tree().get_network_unique_id()))
	player1.set_network_master(get_tree().get_network_unique_id())
	player1.global_transform = player_pos_1.global_transform
	add_child(player1)
	
	var player2 = preload("res://Scenes/MPPlayer2.tscn").instance()
	player2.set_name(str(GameRun.user_id))
	player2.set_network_master(GameRun.user_id)
	player2.global_transform = player_pos_2.global_transform
	add_child(player2)


func _process(delta):
	if GameRun.show_counter == 0:
		$FPSCounter.hide()
	if GameRun.show_counter == 1:
		$FPSCounter.show()
